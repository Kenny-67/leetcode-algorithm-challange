/**
 * @param {string} word
 * @return {boolean}
 */
var detectCapitalUse = function(word) {
    let allCapitals = true;
    let allSmall = true;
    let firstLetterCapital = true;
    for(let i = 0; i<word.length; i++){
        let letter = word[i]
        if(letter !== letter.toUpperCase()){
            allCapitals = false
        }else{
            allSmall = false
        }
        if(letter == letter.toUpperCase() && i>0){
            firstLetterCapital = false
        }
    }
    return allCapitals == true || allSmall == true || firstLetterCapital == true
};