/**
 * @param {string} s
 * @return {number}
 */
var maxPower = function(s) {
    let charCount = 0
    let length = 0
    let prevChar = s[0]

    for(let i = 0; i<s.length; i++){
        let char = s[i]
        if(prevChar == char){
            charCount++
        }else{
            length = Math.max(length, charCount)
            charCount = 1
        }
        if(i == s.length-1){
            length = Math.max(length, charCount)
        }
        prevChar = char
    }
    return length
};