/**
 * @param {number} n
 * @return {string}
 */
var thousandSeparator = function(n) {
    let numString = n.toString()
    let newString = ""
    let charCount = 1
    for(let i = numString.length-1; i>=0; i--){
        if(charCount == 3 && i > 0){
            newString = "." + numString[i] + newString
            charCount = 1
        }else{
            newString = numString[i] + newString
            charCount++
        }
    }
    return newString
};