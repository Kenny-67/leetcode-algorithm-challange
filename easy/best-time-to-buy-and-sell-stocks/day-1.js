/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    let profit = 0
    let minPrice = prices[0]
    for(let i = 0; i<prices.length; i++){
        let price = prices[i]
        profit = Math.max(profit, (price - minPrice))
        minPrice = Math.min(minPrice, price)
    }

    return profit
};