/**
 * @param {number} n
 * @return {boolean}
 */
var isThree = function(n) {
    let divisorCount = 0
    let has3Divisors = true
    for(let i =1; i<=n; i++){
        if(divisorCount > 3) {
            has3Divisors = false
            break
        }
        if(n%i == 0) divisorCount++
    }
    return divisorCount == 3 && has3Divisors
};