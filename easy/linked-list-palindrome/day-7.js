/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function(head) {
    let str = `${head.val}`
    let next = head.next
    while(next != null){
        str+=next.val
        next = next.next
    }
    let reversedString = ""
    for(let i = str.length-1; i>=0; i--){
        let char = str[i]
        reversedString+=char
    }
    return str == reversedString
};