/**
 * @param {string} str1
 * @param {string} str2
 * @return {string}
 */
var gcdOfStrings = function(str1, str2) {
    let x = str1,y=str2
    //set x to be the shortest of str1 and str2, so the shortest will be the divisor
    if(str1.length < str2.length){
        x=str1
        y=str2
    }else{
        x=str2
        y=str1
    }
    //loop from the length of x till 0
    for(let i = x.length; i>0; i--){
        let subStringOfX = x.slice(0,i) // string "ABCABC" and index 5 will become "ABCAB"
        let lenYSubString = y.split(subStringOfX).join("").length 
        /**
         *  After the above operation string= "ABCABCABCABC" and subStringOfX= "ABCAB" would be
         *  ["C", "C"] and joining to become "CC"
         */
        let lenStr1SubString =str1.split(subStringOfX).join("").length
        let lenStr2SubString = str2.split(subStringOfX).join("").length
        if(lenYSubString == 0 && lenStr1SubString == 0 && lenStr2SubString == 0){
            //if they are all equal then x is a divisor
            //return the sub string
            return subStringOfX
        }
    }
    return ""
};