/**
 * @param {number[]} arr
 * @return {boolean}
 */
var checkIfExist = function(arr) {
    let found = false
    let map = {}
    for(let i = 0; i<arr.length; i++){
        let num = arr[i]
        map[num] = i
    }
    for(let i = 0; i<arr.length; i++){
        let num = arr[i] * 2
        if(map[num] !== undefined && map[num] != i){
            found = true
            break;
        }
    }
    return found
};