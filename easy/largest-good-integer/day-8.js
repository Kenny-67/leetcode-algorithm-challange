/**
 * @param {string} num
 * @return {string}
 */
var largestGoodInteger = function(num) {
    let goodInteger = ""
    if(num.length < 2) return goodInteger
    let i = 0
    while(i<num.length-2){
        let pointer1 = num[i]
        let pointer2 = num[i+1]
        let pointer3 = num[i+2]
        if(pointer1 == pointer2 && pointer2 == pointer3){
            let subString = `${pointer1}${pointer2}${pointer3}`
            if(goodInteger.length && +goodInteger[0] > +pointer1){
                i++
                continue
            }
            goodInteger = subString
        }
        i++
    }
    return goodInteger
};