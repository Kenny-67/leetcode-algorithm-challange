/**
 * @param {string} s
 * @return {boolean}
 */
var areNumbersAscending = function(s) {
    let prevNumber = -Infinity
    let value = true
    s.split(" ").forEach(word => {
            if(!isNaN(word)){
            if(+word > prevNumber){
                prevNumber = word
            }else{
                value = false
            }
        }
    })
    return value
};