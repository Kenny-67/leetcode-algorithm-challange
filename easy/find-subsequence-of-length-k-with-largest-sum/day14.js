/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var maxSubsequence = function (nums, k) {
  let newArr = nums.map((num, i) => [num, i]);
  newArr.sort((a, b) => a[0] - b[0]);
  let sum = newArr.slice(newArr.length - k);
  sum.sort((a, b) => a[1] - b[1]);
  return sum.map((num) => num[0]);
};
