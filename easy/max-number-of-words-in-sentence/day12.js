/**
 * @param {string[]} sentences
 * @return {number}
 */
var mostWordsFound = function(sentences) {
    let wordCount = 0
    for(const sentence of sentences){
        wordCount = Math.max(sentence.split(" ").length, wordCount)
    }

    return wordCount
};