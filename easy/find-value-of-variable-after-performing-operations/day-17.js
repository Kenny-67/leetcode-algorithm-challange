/**
 * @param {string[]} operations
 * @return {number}
 */
var finalValueAfterOperations = function(operations) {
    return operations.reduce((accumulator, current) => {
        if(current == "++X" || current == "X++"){
            return accumulator + 1
        }else{
            return accumulator - 1
        }
    }, 0)
};