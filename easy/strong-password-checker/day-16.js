/**
 * @param {string} password
 * @return {boolean}
 */
var strongPasswordCheckerII = function(password) {
    let isStrongPassword = true
    let prevChar = ""
    let hasLowerCase = false
    let hasUpperCase = false
    let hasSpecialChar = false
    let hasOneDigit = false

    let specialCharacterSet = new Set(["!","@","#","$","%","^","&","*","(",")","-","+"])
    if(password.length < 8) return false
    for(let i =0; i<password.length; i++){
        let char = password[i]
        if(/^[a-zA-Z]$/.test(char) && char == char.toUpperCase()) hasUpperCase = true
        if(/^[a-zA-Z]$/.test(char) && char == char.toLowerCase()) hasLowerCase = true
        if(/^[0-9]$/.test(char)) hasOneDigit = true
        if(specialCharacterSet.has(char)) hasSpecialChar = true
        if(prevChar == char){
            isStrongPassword = false
            break
        }
        prevChar = char
    }
    return isStrongPassword && hasLowerCase && hasUpperCase && hasSpecialChar && hasOneDigit
};