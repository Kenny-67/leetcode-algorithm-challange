/**
 * @param {string} jewels
 * @param {string} stones
 * @return {number}
 */
var numJewelsInStones = function (jewels, stones) {
  let jewelsSet = new Set();
  for (let i = 0; i < jewels.length; i++) {
    let char = jewels[i];
    jewelsSet.add(char);
  }
  let jewelsCount = 0;
  for (let i = 0; i < stones.length; i++) {
    let char = stones[i];
    if (jewelsSet.has(char)) jewelsCount++;
  }

  return jewelsCount;
};
